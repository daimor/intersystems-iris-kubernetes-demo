import { Component, OnInit, HostListener, ViewEncapsulation } from '@angular/core';
import { AuthService, UserInfo } from '../auth/auth.service';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { MenuItem } from '@app/shared';


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MainComponent implements OnInit {

  userInfo: UserInfo;

  menuItems: MenuItem[];

  public options = {
    position: ['bottom', 'right'],
    timeOut: 5000,
    lastOnBottom: true,
    clickToClose: true
  };

  constructor(
    private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
  }

  ngOnInit() {
    this.menuItems = [{
      title: 'List',
      icon: 'fa-th',
      link: ['home'],
      activeLink: ['hidden']
    }, {
      title: 'Create note',
      icon: 'fa-plus',
      link: ['new'],
      activeLink: ['hidden']
    }];
    this.authService
      .userInfo
      .subscribe(userInfo => {
        this.userInfo = userInfo;
      });
  }

  public hide(el: any, visibility) {
    if (el) {
      el.style.visibility = visibility ? null : 'hidden';
    }
  }

}
