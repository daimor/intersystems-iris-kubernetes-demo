import { Component, ViewEncapsulation, ElementRef, ViewContainerRef, Inject, LOCALE_ID, Renderer2 } from '@angular/core';
import { Router } from '@angular/router';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [
    './app.component.scss'
  ],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {
  title = 'DemoApp';

  constructor(
    @Inject(DOCUMENT) doc: Document,
    @Inject(LOCALE_ID) locale: string,
    renderer: Renderer2
  ) {
    renderer.setAttribute(doc.documentElement, 'lang', locale);
  }
}
