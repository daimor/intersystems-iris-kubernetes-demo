import { Component, OnInit, Input, ContentChild, TemplateRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-side-panel',
  templateUrl: './side-panel.component.html',
  styleUrls: ['./side-panel.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SidePanelComponent implements OnInit {

  @ContentChild(TemplateRef) contentPattern: TemplateRef<any>;

  @ViewChild('modal') modal: TemplateRef<any>;

  @Input() title: string;

  // data for dialog content, such as forms
  private data: any;

  private modalRef: NgbModalRef;

  constructor(
    private modalService: NgbModal
  ) { }

  ngOnInit() {
  }

  public show(data?: any): void {
    this.data = { ...data };

    this.modalRef = this.modalService.open(this.modal, {
      windowClass: 'side-panel'
    });
  }

  public hide(event?: Event): void {
    if (this.modalRef) {
      this.modalRef.dismiss();
    }
  }
}
