import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter
} from '@angular/core';

@Component({
  selector: 'app-headerbuttons',
  templateUrl: './header-buttons.component.html',
  styleUrls: ['./header-buttons.component.scss']
})
export class HeaderButtonsComponent implements OnInit {
  @Input () buttons: any[];
  @Output() onBtnClick = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  btnClick(pagedata, idx) {
    for ( let btn of this.buttons) {
      btn.focus = false;
    }
    if ( this.buttons[idx] ) {
      this.buttons[idx].focus = true;
    }
    this.onBtnClick.emit(pagedata);
  }

}
