import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { ToolbarComponent } from './toolbar.component';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgbDropdownModule,
    SharedModule
  ],
  declarations: [
    ToolbarComponent
  ]
})
export class ToolbarModule { }
