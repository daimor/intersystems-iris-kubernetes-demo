import {
  Component,
  Directive,
  ElementRef,
  forwardRef,
  Renderer,
  AfterViewInit,
  AfterContentInit,
  HostBinding,
  Input,
  Output,
  EventEmitter,
  OnInit,
  OnDestroy,
  ChangeDetectorRef,
  ViewChild,
  ViewEncapsulation,
  HostListener
} from '@angular/core';
import {
  Router,
  Routes,
  ActivatedRoute,
  RouteConfigLoadEnd,
  RoutesRecognized
} from '@angular/router';

import { NavbarService } from './navbar.service';
import { AuthService } from '@app/auth';
import { UserInfo } from '@app/auth/auth.service';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Directive({
  selector: '[appCollapsable]'
})
export class CollapsableDirective implements OnInit, AfterViewInit {

  @Input('appCollapsable') enabled: boolean;

  @HostBinding('class.collapsable') collapsable;

  @HostBinding('class.open') open;

  constructor(
    private el: ElementRef,
    private renderer: Renderer
  ) {
  }

  ngOnInit() {
    this.collapsable = this.enabled;
  }

  ngAfterViewInit() {
    if (!this.enabled) {
      return;
    }
    let toggleLink = this.el.nativeElement.children[0];
    let chevron = this.renderer.createElement(toggleLink, 'span');
    this.renderer.setElementClass(chevron, 'chevron', true);
    this.renderer.setElementClass(chevron, 'fa', true);
  }

  @HostListener('click')
  toggle() {
    if (this.enabled) {
      this.open = !this.open;
    }
  }
}

export class MenuItem {
  title?: string;
  class?: string[];
  icon?: string;
  children?: MenuItem[];
  link?: string[];
  queryParams?: any;
  data?: any;
  group?: string;
  onClick?: (val: any) => void;
  onClose?: () => void;
  selected?: boolean;
  activeLink?: string[];
}

export class NavbarSelectItem {
  id: string | number;
  text: string;
}

@Component({
  selector: '[app-navbar-list]',
  templateUrl: 'navbar-list.directive.html'
})
export class NavbarListComponent implements OnInit, AfterContentInit, OnDestroy {

  @Input() level;

  @Input() items: MenuItem[];

  @Input() path: string[];

  isRoot: boolean;

  private subscription: any;

  constructor(
    private service: NavbarService,
    private cd: ChangeDetectorRef
  ) {
  }

  ngOnInit() {
    this.level = this.level || 0;
    this.isRoot = this.level === 0;
    this.items.forEach(el => {
      if (el.activeLink) { return; }
      el.activeLink = [ this.isRoot ? 'active' : 'current-page' ];
      if (el.group) {
        el.children = this.service.getItems(el.group, el.link);
      }
      if (el.children && el.children.length > 0) {
        el.activeLink.push('open');
      }
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  ngAfterContentInit() {
    this.subscription = this.service.navChange.subscribe(group => {
      let item = this.items.find(el => el.group === group);
      item.children = this.service.getItems(group, item.link);
      this.cd.detectChanges();
    });
  }

  close(item: MenuItem) {
    item.onClose();
    return false;
  }
}

@Component({
  selector: 'app-navbar',
  template: `<ul *ngIf="items" app-navbar-list [items]="items"></ul>`,
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {

  visible: boolean = true;

  @Input() items: MenuItem[];

  constructor(
  ) {
  }

  findNavRoutes(appRoutes): Routes {
    let routes: Routes = [];

    return routes;
  }
}
