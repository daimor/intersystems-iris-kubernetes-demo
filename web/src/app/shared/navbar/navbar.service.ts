import { MenuItem } from '@app/shared';
import { Injectable, EventEmitter } from '@angular/core';
import { UrlSegment, ActivatedRouteSnapshot } from '@angular/router';

@Injectable()
export class NavbarService {

  navChange: EventEmitter<string> = new EventEmitter();

  private pages: any;

  workPosition: EventEmitter<string> = new EventEmitter();

  constructor() {
    this.pages = [];
  }

  open(group: string, id: string, data: any, title: string): Promise<any> {
    return new Promise((resolve, reject) => {
      if (id) {
        this.pages[group] = this.pages[group] || [];
        let result = this.pages[group].find(el => el.id === id);
        if (result) {
          resolve(result.data);
        } else {
          this.pages[group].push({
            id,
            title,
            data
          });
          resolve(data);
        }
        this.navChange.emit(group);
        return;
      }
      reject();
    });
  }

  close(group: string, id: string) {
    if (this.pages[group]) {
      let index = this.pages[group].findIndex(el => el.id === id);
      if (index > -1) {
        this.pages[group].splice(index, 1);
        this.navChange.emit(group);
      }
    }
  }

  closeAll(group) {
    delete this.pages[group];
    this.navChange.emit(group);
  }

  getItems(group: string, path: string[]): MenuItem[] {
    let items = [];
    if (this.pages[group]) {
      items = this.pages[group].map((el, id) => {
        return {
          title: el.title,
          link: [...path, el.id],
          onClose: () => { this.close(group, el.id); },
          activeLink: ['active']
        };
      });

      if (items.length) {
        items.unshift({
          id: 'closeAll',
          title: 'Zrušit vše',
          onClose: () => { this.closeAll(group); },
          activeLink: []
        });
      }
    }
    return items;
  }

  saveWorkPosition(workPosition) {
    this.workPosition.emit(workPosition);
  }
}
