import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params, ParamMap } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  model: any = {};

  loading: boolean = false;

  error: string = '';

  hide: boolean;

  constructor(
    public authService: AuthService,
    private route: ActivatedRoute,
    public router: Router
  ) {
  }

  ngOnInit() {
  }

  login() {
    this.authService
      .login(this.model.username, this.model.password)
      .subscribe(() => {
        this.router.navigate(['/']);
      }, error => {
        this.error = 'Username or password is incorrect';
        this.loading = false;
      });
  }

  logout() {
    this.authService.logout();
  }

}
