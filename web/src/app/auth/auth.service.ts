import { Injectable, Injector, Inject, forwardRef } from '@angular/core';
import { PlatformLocation } from '@angular/common';
import {
  Router,
  Route,
  ActivatedRoute,
  Params,
  CanActivate,
  CanLoad,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import {
  HttpClient,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpEvent,
  HttpHeaders
} from '@angular/common/http';
import { Observable } from 'rxjs/Rx';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ReplaySubject } from 'rxjs/ReplaySubject';

export class UserInfo {
  username: string;
}

@Injectable()
export class AuthService {
  client_id = 'client';

  client_secret = 'secret';

  authApiUrl: string = 'api/demoapp/';

  private _token: BehaviorSubject<string> = new BehaviorSubject<string>(null);

  private _userInfo: ReplaySubject<UserInfo>;

  public userInfo: ReplaySubject<UserInfo> = new ReplaySubject<UserInfo>(1);

  constructor(private http: HttpClient, private router: Router, private route: ActivatedRoute) {
    document.execCommand('ClearAuthenticationCache', false);
    this._token
      .asObservable()
      .distinctUntilChanged()
      .subscribe(token => {
        if (token) {
          this.getUserInfo();
        }
      });
  }

  public authenticated(): boolean {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    const token = currentUser && currentUser.token;
    if (token) {
      this._token.next(token);
      return true;
    }
    return false;
  }

  getToken(): string {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    const token = currentUser && currentUser.token;
    return token;
  }

  public login(username: string, password: string): Observable<string> {
    let basicheader = btoa(encodeURI(`${username}:${password}`));

    let headers = new HttpHeaders();

    headers = headers.set('Authorization', 'Basic ' + basicheader);
    headers = headers.set('Cache-Control', 'no-cache');

    return this.http
      .options<any>(this.authApiUrl, {
        headers
      })
      .map(data => {
        let token = `Basic ${basicheader}`;
        localStorage.setItem('currentUser', JSON.stringify({ username, token }));
        this._token.next(token);
        return username;
      });
  }

  public logout(): void {
    localStorage.removeItem('currentUser');
    localStorage.removeItem('currentUserSettings');
    // this.zoneImpl.run(() => this.user = null);
    this.router.navigate(['login']);
  }

  private getUserInfo() {
    this.http.get<any>(`${this.authApiUrl}userinfo`).subscribe(
      data => {
        this.userInfo.next({
          username: data.preferred_username,
          ...data
        });
      },
      error => {
        this.logout();
        return Observable.throw(error);
      }
    );
  }
}

@Injectable()
export class AuthGuardService implements CanActivate, CanLoad {
  constructor(private _platformLocation: PlatformLocation, private authService: AuthService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> {
    return this.checkLogin(state.url);
  }

  checkLogin(url: string): boolean | Observable<boolean> {
    if (this.authService.authenticated()) {
      return true;
    }

    let queryParams = this.router.parseUrl(this._platformLocation.search).queryParams;
    this.router.navigate(['/login'], { queryParams });
    return false;
  }

  canLoad(route: Route): boolean | Observable<boolean> {
    let url = `/${route.path}`;
    return this.checkLogin(url);
  }

  canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    return true;
  }
}

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  private isRefreshingToken = false;
  tokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);

  constructor(private authService: AuthService) {}

  setHeaders(req: HttpRequest<any>) {
    if (!req.headers.has('Authorization')) {
      req = req.clone({
        setHeaders: { Authorization: this.authService.getToken() }
      });
    }

    if (!req.headers.has('Accept')) {
      req = req.clone({
        setHeaders: { Accept: 'application/json' }
      });
    }

    if (req.body && !req.headers.has('Content-Type')) {
      req = req.clone({
        setHeaders: { 'Content-Type': 'application/json' }
      });
    }

    return req;
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(this.setHeaders(req));
  }
}
