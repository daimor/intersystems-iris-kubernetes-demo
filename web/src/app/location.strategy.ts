import { Injectable, Optional, Inject } from '@angular/core';
import { Location, LocationStrategy, PlatformLocation, APP_BASE_HREF } from '@angular/common';

@Injectable()
export class NoneLocationStrategy extends LocationStrategy {

  private _history: any[];

  private _baseHref: string = '';

  constructor(
    private _platformLocation: PlatformLocation,
    @Optional() @Inject(APP_BASE_HREF) _baseHref?: string) {
    super();
    this._history = [];
    if (_baseHref) {
      this._baseHref = _baseHref;
    }
  }

  onPopState(fn: any): void {
    this._platformLocation.onPopState(fn);
    this._platformLocation.onHashChange(fn);
  }

  getBaseHref(): string { return this._baseHref; }

  prepareExternalUrl(internal: string): string {
    return Location.joinWithSlash(this._baseHref, internal);
  }

  path(includeHash = false): string {
    if (window.history.state && window.history.state) {
      let url = window.history.state.url;
      return url;
    }
    const pathname = this._platformLocation.pathname +
      Location.normalizeQueryParams(this._platformLocation.search);
    const hash = this._platformLocation.hash;
    return hash && includeHash ? `${pathname}${hash}` : pathname;
  }

  pushState(state: any, title: string, url: string, queryParams: string) {
    state = {
      url: url,
      queryParams: queryParams
    };
    this._platformLocation.pushState(state, title, '');
  }

  replaceState(state: any, title: string, url: string, queryParams: string) {
    state = {
      url: url,
      queryParams: queryParams
    };
    this._platformLocation.replaceState(state, title, '');
  }

  forward(): void { this._platformLocation.forward(); }

  back(): void { this._platformLocation.back(); }
}
