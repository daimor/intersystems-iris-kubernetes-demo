// import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID, Injectable } from '@angular/core';
import { CommonModule, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { ServiceWorkerModule } from '@angular/service-worker';
import { AppComponent } from './app.component';
import { BaseRequestOptions, RequestOptions, RequestOptionsArgs, URLSearchParams } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

import { NoneLocationStrategy } from './location.strategy';

// this import BrowserModule
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app.routing';
import { AuthModule } from '@app/auth';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/toPromise';

import { environment } from '@env/environment';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserAnimationsModule,
    AppRoutingModule,
    CommonModule,
    HttpClientModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    NgbModule.forRoot(),
    AuthModule.forRoot(),
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'en' },
    { provide: LocationStrategy, useClass: environment.production ? NoneLocationStrategy : PathLocationStrategy }
  ],
  entryComponents: [AppComponent],
  bootstrap: [AppComponent]
})
export class AppModule {

}
