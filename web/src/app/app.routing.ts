import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuardService } from './auth/';

import {
  loginRoutes,
  authProviders
} from './auth/login/';

const appRoutes: Routes = [
  ...loginRoutes,
  {
    path: 'main',
    canLoad: [AuthGuardService],
    canActivateChild: [AuthGuardService],
    loadChildren: './main/main.module#MainModule'
  }, {
    path: '',
    redirectTo: 'main',
    pathMatch: 'full'
  }, {
    path: '**',
    redirectTo: 'main'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes, { enableTracing: false })],
  exports: [RouterModule],
  providers: [authProviders]
})
export class AppRoutingModule { }
