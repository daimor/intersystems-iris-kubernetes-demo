Class App.REST Extends App.REST.Base
{

XData UrlMap [ XMLNamespace = "http://www.intersystems.com/urlmap" ]
{
<Routes>
  <Route Url="/" Method="GET" Call="GetInfo" Cors="true"/>
  <Route Url="/_ping" Method="GET" Call="Ping" Cors="true"/>

  <Route Url="/userinfo" Method="GET" Call="UserInfo" Cors="true"/>

  <Route Url="/notes" Method="GET" Call="NotesList" Cors="true"/>
  <Route Url="/notes/:id" Method="GET" Call="NotesDetails" Cors="true"/>
  <Route Url="/notes" Method="PUT" Call="NotesAdd" Cors="true"/>
  <Route Url="/notes/:id" Method="DELETE" Call="NotesDelete" Cors="true"/>
</Routes>
}

ClassMethod GetInfo()
{
  set info = {
    "version": ($ZVERSION),
    "system": ($SYSTEM),
    "username": ($USERNAME),
    "roles": ($ROLES)
  }
  return ..%ProcessResult($$$OK, info)
}

ClassMethod Ping() As %Status
{
  set pong = {
    "message": "ping"
  }
  quit ..%ProcessResult($$$OK, pong)
}

ClassMethod UserInfo() As %Status
{
  new $NAMESPACE
  set $NAMESPACE = "%SYS"
  do ##class(Security.Users).Get($USERNAME, .props)
  set result = {
    "username": ($USERNAME),
    "roles": ($ROLES),
    "name": ($GET(props("FullName")))
  }
  quit ..%ProcessResult($$$OK, result)
}

ClassMethod NotesList() As %Status
{
  set user = $USERNAME
  if user = "Admin" set user = ""
  set rs = ##class(App.Data.Notes).ListFunc(user)
  set list = []
  while rs.%Next() {
    set note = {
      "id": (rs.ID),
      "title": (rs.Title),
      "date": (rs.TimeStamp)
    }
    do list.%Push(note)
  }
  quit ..%ProcessResult($$$OK, list)
}

ClassMethod NotesDetails(id) As %Status
{
  set note = ##class(App.Data.Notes).%OpenId(id)
  set result = {
    "title": (note.Title),
    "text": (note.Text),
    "date": (note.TimeStamp)
  }
  quit ..%ProcessResult($$$OK, result)
}

ClassMethod NotesAdd() As %Status
{
  set json = %request.Content
  set note = ##class(App.Data.Notes).%New()
  set note.Title = json.title
  set note.Text = json.text
  set note.TimeStamp = $ZDATETIME($NOW(), 3)
  set note.Author = $USERNAME

  quit ..%ProcessResult(note.%Save())
}

ClassMethod NotesDelete(id) As %Status
{
  quit ..%ProcessResult(##class(App.Data.Notes).%DeleteId(id))
}

}
